ABOUT
------

Module that integrates Flo2Cash payment gateway with Drupal Commerce
allowing customers to pay their orders using the Flo2Cash payment gateway.

REQUIREMENTS
------------

Required : Drupal Commerce


INSTALLATION
------------

1. Download and enable the module.

2. Configure the Flo2Cash payment gateway:

   Go to Payment methods page: admin/commerce/config/payment-methods
   Click on edit link from "Flo2Cash" payment method. Then,
   Edit the rule action "Enable payment method: Flo2Cash" in
   order to access to the payment gateway settings.

   The only setting you need to fulfill is the Flo2Cash Account ID which is
   the credential given by Flo2Cash. 

3. Enjoy !

   Now you should be able to expose the Flo2Cash payment method.
